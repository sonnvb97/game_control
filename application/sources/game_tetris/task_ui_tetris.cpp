#include "task_ui_tetris.h"
#include "app.h"

#include "task_gc_tetris.h"
#include "task_list.h"

#include "Adafruit_ssd1306syp.h"
#include "message.h"
#include "task.h"

static void Tetris_Graphic_Init();
//static void Tetris_Graphic_DeInit();
static void Tetris_Graphic_Update();
static void Tetris_Graphic_GameOver();

static void Other_GraphicUpdate();
static void MovingObject_GraphicUpdate();
static void StandingObject_GraphicUpdate();

Adafruit_ssd1306syp GraphicObject;

void task_ui_tetris(ak_msg_t *message)
{
	switch(message->sig)
	{
		case TETRIS_UI_INIT_REQ:
			Tetris_Graphic_Init();
			task_post_pure_msg(TETRIS_TASK_SM_ID, GAME_SM_INIT_RES_OK);
			break;
		case TETRIS_UI_GAME_START:
			Tetris_Graphic_Update();
			break;
		case TETRIS_UI_UPDATE:
			Tetris_Graphic_Update();
			break;
		case TETRIS_UI_GAME_OVER:
			Tetris_Graphic_GameOver();
			break;
	}
}

void Other_GraphicUpdate()
{
	GraphicObject.drawLine(TETRIS_BOUNDARY_COLUMN + 1, 0, TETRIS_BOUNDARY_COLUMN + 1, 62, WHITE);
	GraphicObject.drawLine(TETRIS_BOUNDARY_COLUMN + 1, 62, TETRIS_BOUNDARY_COLUMN + 43, 62, WHITE);
	GraphicObject.drawLine(TETRIS_BOUNDARY_COLUMN + 43, 62, TETRIS_BOUNDARY_COLUMN + 43, 0, WHITE);

	GraphicObject.setTextSize(1);
	GraphicObject.setTextColor(WHITE);
	GraphicObject.setCursor(0, 0);
	GraphicObject.print("Score");
	GraphicObject.setCursor(0, 12);
	GraphicObject.print(Tetris_Score);

	GraphicObject.setCursor(0, 27);
	GraphicObject.print("Lv: ");
	GraphicObject.print(Tetris_Level);

	GraphicObject.setCursor(0, 42);
	GraphicObject.print("Line");
	GraphicObject.setCursor(0, 54);
	GraphicObject.print(Tetris_Line);

	GraphicObject.setCursor(100, 5);
	GraphicObject.print("Next");

	GraphicObject.drawRect(TETRIS_NEXTBOX_COLUMN, TETRIS_NEXTBOX_ROW, 29, 39, WHITE);
	for(int i = 0; i<3; i++)
	{
		switch(MovingObject.NextTetromino[i])
		{
			case I_Block:
			{
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  2, TETRIS_NEXTBOX_ROW + 5 + 11*i + 2, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  6, TETRIS_NEXTBOX_ROW + 5 + 11*i + 2, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 + 10, TETRIS_NEXTBOX_ROW + 5 + 11*i + 2, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 + 14, TETRIS_NEXTBOX_ROW + 5 + 11*i + 2, 3, 3, WHITE);
			}
			break;
			case O_Block:
			{
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  6, TETRIS_NEXTBOX_ROW + 5 + 11*i + 0, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 + 10, TETRIS_NEXTBOX_ROW + 5 + 11*i + 0, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  6, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 + 10, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
			}
			break;
			case L_Block:
			{
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 + 12, TETRIS_NEXTBOX_ROW + 5 + 11*i + 0, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  4, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  8, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 + 12, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
			}
			break;
			case J_Block:
			{
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  4, TETRIS_NEXTBOX_ROW + 5 + 11*i + 0, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  4, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  8, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 + 12, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
			}
			break;
			case Z_Block:
			{
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  4, TETRIS_NEXTBOX_ROW + 5 + 11*i + 0, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  8, TETRIS_NEXTBOX_ROW + 5 + 11*i + 0, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  8, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 + 12, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
			}
			break;
			case S_Block:
			{
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  8, TETRIS_NEXTBOX_ROW + 5 + 11*i + 0, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 + 12, TETRIS_NEXTBOX_ROW + 5 + 11*i + 0, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  4, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  8, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
			}
			break;
			case T_Block:
			{
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  8, TETRIS_NEXTBOX_ROW + 5 + 11*i + 0, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  4, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 +  8, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
				GraphicObject.fillRect(TETRIS_NEXTBOX_COLUMN + 5 + 12, TETRIS_NEXTBOX_ROW + 5 + 11*i + 4, 3, 3, WHITE);
			}
			break;
		}
	}
}

void MovingObject_GraphicUpdate()
{
	Coordinate_t _Temp;
	for(int i = 0; i<4; i++)
	{
		tetris_coordinate_to_position(MovingObject.CurrentPoint[i], &_Temp);
		GraphicObject.fillRect(_Temp.Column, _Temp.Row, 3, 3, WHITE);
		tetris_coordinate_to_position(MovingObject.DropPoint[i], &_Temp);
		GraphicObject.fillRect(_Temp.Column+1, _Temp.Row+1, 1, 1, WHITE);
	}
}

void StandingObject_GraphicUpdate()
{
	Coordinate_t _Temp, _TempStandingObj;
	for(int16_t row = 0; row<TETRIS_ROW_NUMBER; row++)
	{
		for(int16_t column = 0; column<TETRIS_COLUMN_NUMBER; column++)
		{
			if( (StandingObject.Row[row] & (1<<(uint16_t)column) )!= 0x00)
			{
				_TempStandingObj.Row = row;
				_TempStandingObj.Column = column;
				tetris_coordinate_to_position(_TempStandingObj, &_Temp);
				GraphicObject.fillRect(_Temp.Column, _Temp.Row, 3, 3, WHITE);
			}
		}
	}
}

void Tetris_Graphic_Init()
{
	GraphicObject.initialize();
	GraphicObject.display_off();
	GraphicObject.display_on();

	GraphicObject.setTextSize(2);
	GraphicObject.setTextColor(WHITE);
	GraphicObject.setCursor(10, 25);
	GraphicObject.println("Press any button!");
	GraphicObject.update();
}

//void Tetris_Graphic_DeInit()
//{
//	GraphicObject.clear(0);
//	GraphicObject.display_off();
//}

void Tetris_Graphic_Update()
{
	// Clear all Objects
	GraphicObject.clear();

	// Update all Objects
	MovingObject_GraphicUpdate();
	StandingObject_GraphicUpdate();
	Other_GraphicUpdate();

	// Show all Objects
	GraphicObject.update();
}

void Tetris_Graphic_GameOver()
{
	GraphicObject.clear(0);
	GraphicObject.setTextSize(2);
	GraphicObject.setTextColor(WHITE);
	GraphicObject.setCursor(10, 25);
	GraphicObject.println("GAME OVER");
	GraphicObject.update();
}

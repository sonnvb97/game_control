#include "Tetris_StandingObject.h"
#include "Tetris_MovingObject.h"
#include <string.h>
#include "Tetris_GameHandler.h"

static uint8_t isFilled(TetrisRow_t Row);
static uint8_t StandingObject_isExisted(Coordinate_t Coordinate);
static int StandingObject_Init();
static int StandingObject_DeInit();
static int StandingObject_Update();

uint8_t isFilled(TetrisRow_t Row)
{
	return(Row == TETRIS_ROW_FILLED_FLAG);
}

StandingObject_t StandingObject =
{
	StandingObject_Init,
	StandingObject_DeInit,
	StandingObject_Update,

	StandingObject_isExisted

};

int StandingObject_Init()
{
	memset(StandingObject.Row, 0, sizeof(TetrisRow_t)*(TETRIS_ROW_NUMBER+4));
	return 0;
}

int StandingObject_DeInit()
{
	return 0;
}

int StandingObject_Update()
{
	uint8_t line_clear = 0;

	for(int i = 0; i < 4; i++)
	{
		StandingObject.Row[MovingObject.CurrentPoint[i].Row] |= 1<<((uint16_t)MovingObject.CurrentPoint[i].Column);
	}

	for(int i = 0; i < TETRIS_ROW_NUMBER; i++)
	{
		if(isFilled(StandingObject.Row[i]))
		{
			line_clear++;
			for(int j = i; j < TETRIS_ROW_NUMBER + 4 - 1; j++)
			{
				StandingObject.Row[j] = StandingObject.Row[j+1];
			}
			StandingObject.Row[TETRIS_ROW_NUMBER + 3] = 0;
			i--;
		}
	}

	switch(line_clear)
	{
		case 1:
			Tetris_Score += 20;
			break;
		case 2:
			Tetris_Score += 60;
			break;
		case 3:
			Tetris_Score += 100;
			break;
		case 4:
			Tetris_Score += 150;
			break;

	}

	Tetris_Line += line_clear;
	Level_Update();
	line_clear = 0;

	if(StandingObject.Row[TETRIS_ROW_NUMBER] != 0x0000)
	{
		return 1;	//Game Over!!
	}
	else
	{
		return 0;
	}
}

uint8_t StandingObject_isExisted(Coordinate_t Coordinate)
{
	TetrisRow_t Temp = 1<<((uint16_t)Coordinate.Column);
	if( (StandingObject.Row[Coordinate.Row] & Temp) != 0x00)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

#include "fsm.h"
#include "port.h"
#include "message.h"

#include "app_bsp.h"
#include "task_sm_tetris.h"
#include "task_my_sm.h"
#include "task_list.h"
#include "app.h"
#include "timer.h"

#include "my_debug.h"

void task_sm_tetris_playing(ak_msg_t *message)
{
	switch(message->sig)
	{
		case INPUT_SM_BUTTON_LEFT_PRESSED:
			if(btn_mode.state != BUTTON_SW_STATE_RELEASED)
			{
				task_post_pure_msg(TETRIS_TASK_GC_ID, TETRIS_GC_ROTATE);
			}
			else
			{
				task_post_pure_msg(TETRIS_TASK_GC_ID, TETRIS_GC_GO_LEFT);
			}
			break;
		case INPUT_SM_BUTTON_RIGHT_PRESSED:
			if(btn_mode.state != BUTTON_SW_STATE_RELEASED)
			{
				task_post_pure_msg(TETRIS_TASK_GC_ID, TETRIS_GC_GO_DOWN);
			}
			else
			{
				task_post_pure_msg(TETRIS_TASK_GC_ID, TETRIS_GC_GO_RIGHT);
			}
			break;
		case GAME_SM_GAME_OVER:
			task_sm_pointer = task_sm_tetris_gameover;
			break;
	}
}

void task_sm_tetris_gameover(ak_msg_t *message)
{
	switch(message->sig)
	{
		case INPUT_SM_BUTTON_MODE_PRESSED:
		case INPUT_SM_BUTTON_LEFT_PRESSED:
		case INPUT_SM_BUTTON_RIGHT_PRESSED:
			task_sm_pointer = task_sm_ready;
			task_post_pure_msg(TETRIS_TASK_GC_ID, TETRIS_GC_INIT_REQ);
			break;
	}
}

#include "Tetris_MovingObject.h"
#include "Tetris_StandingObject.h"
#include <string.h>

//typedef enum
//{
//	UP,
//	DOWN,
//	LEFT,
//	RIGHT
//} MO_Direction_t;

//static int CheckForCollision(Coordinate_t *CheckingBlock, MO_Direction_t Direction);
static int MovingObject_Init();
static int MovingObject_DeInit();
static int MovingObject_Generate();
static int MovingObject_MoveLeft();
static int MovingObject_MoveRight();
static int MovingObject_MoveDown();
static int MovingObject_Rotate();
static int MovingObject_Check();
static int MovingObject_CheckAbleMove();
static int MovingObject_CalculateDropPlace();

MovingObject_t MovingObject =
{
	MovingObject_Init,
	MovingObject_DeInit,
	MovingObject_Generate,
	MovingObject_MoveLeft,
	MovingObject_MoveRight,
	MovingObject_MoveDown,
	MovingObject_Rotate,
	MovingObject_Check,
	MovingObject_CheckAbleMove,
	MovingObject_CalculateDropPlace,
	{0},			// NextTetromino[3];
	{ {0, 0} },		// CurrentPoint[4];
	{0, 0},			// CenterPoint;
	{ {0, 0} },		// DropPoint[4];
	1,				// able_moveLeft;
	1,				// able_moveRight;
	1				// able_moveDown;
};

Tetromino_t BlockBag1[7];
Tetromino_t BlockBag2[7];

Tetromino_t *CurrentBag, *NextBag;
int Current_Bag_No;
void BlockBag_Rand(Tetromino_t *Bag)
{
	Tetromino_t _TempBag[7] = {I_Block, J_Block, L_Block, O_Block, S_Block, T_Block, Z_Block};
	for(int i = 0; i<7; i++)
	{
		int _Shape = rand()%(7-i)+i;
		Tetromino_t _TempShape = _TempBag[_Shape];
		for(int j = _Shape; j > i; j--)
		{
			_TempBag[j] = _TempBag[j-1];
		}
		_TempBag[i] = _TempShape;
	}
	memcpy(Bag, _TempBag, sizeof(Tetromino_t)*7);
}

int MovingObject_Init()
{
	CurrentBag = BlockBag1;
	NextBag = BlockBag2;
	BlockBag_Rand(CurrentBag);
	while(CurrentBag[0] == O_Block || CurrentBag[0] == S_Block || CurrentBag[0] == Z_Block)
	{
		BlockBag_Rand(CurrentBag);
	}
	BlockBag_Rand(NextBag);
	MovingObject.NextTetromino[0] = CurrentBag[0];
	MovingObject.NextTetromino[1] = CurrentBag[1];
	MovingObject.NextTetromino[2] = CurrentBag[2];
	Current_Bag_No = 2;
	MovingObject.Generate();
	return 0;
}

int MovingObject_DeInit()
{
//	MovingObject.GraphicObj.clear(0);
//	MovingObject.GraphicObj.display_off();
	return 0;
}

int MovingObject_Generate()
{
	switch(MovingObject.NextTetromino[0])
	{
		case I_Block:
		{
			MovingObject.CurrentPoint[0].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2-2);
			MovingObject.CurrentPoint[0].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CurrentPoint[1].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2-1);
			MovingObject.CurrentPoint[1].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CurrentPoint[2].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[2].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CurrentPoint[3].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2+1);
			MovingObject.CurrentPoint[3].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CenterPoint.Column		= (uint16_t)(TETRIS_COLUMN_NUMBER/2) - 0.5;
			MovingObject.CenterPoint.Row			= (uint16_t)(TETRIS_ROW_NUMBER) + 0.5;
		}
		break;
		case O_Block:
		{
			MovingObject.CurrentPoint[0].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2-1);
			MovingObject.CurrentPoint[0].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CurrentPoint[1].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2-1);
			MovingObject.CurrentPoint[1].Row		= (uint16_t)(TETRIS_ROW_NUMBER+1);
			MovingObject.CurrentPoint[2].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[2].Row		= (uint16_t)(TETRIS_ROW_NUMBER+1);
			MovingObject.CurrentPoint[3].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[3].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CenterPoint.Column		= (uint16_t)(TETRIS_COLUMN_NUMBER/2) - 0.5;
			MovingObject.CenterPoint.Row			= (uint16_t)(TETRIS_ROW_NUMBER) + 0.5;
		}
		break;
		case L_Block:
		{
			MovingObject.CurrentPoint[0].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2-1);
			MovingObject.CurrentPoint[0].Row		= (uint16_t)(TETRIS_ROW_NUMBER+2);
			MovingObject.CurrentPoint[1].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2-1);
			MovingObject.CurrentPoint[1].Row		= (uint16_t)(TETRIS_ROW_NUMBER+1);
			MovingObject.CurrentPoint[2].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2-1);
			MovingObject.CurrentPoint[2].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CurrentPoint[3].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[3].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CenterPoint.Column		= (uint16_t)(TETRIS_COLUMN_NUMBER/2 - 1);
			MovingObject.CenterPoint.Row			= (uint16_t)(TETRIS_ROW_NUMBER+1);
		}
		break;
		case J_Block:
		{
			MovingObject.CurrentPoint[0].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[0].Row		= (uint16_t)(TETRIS_ROW_NUMBER+2);
			MovingObject.CurrentPoint[1].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[1].Row		= (uint16_t)(TETRIS_ROW_NUMBER+1);
			MovingObject.CurrentPoint[2].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[2].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CurrentPoint[3].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2-1);
			MovingObject.CurrentPoint[3].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CenterPoint.Column		= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CenterPoint.Row			= (uint16_t)(TETRIS_ROW_NUMBER+1);
		}
		break;
		case Z_Block:
		{
			MovingObject.CurrentPoint[0].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2+1);
			MovingObject.CurrentPoint[0].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CurrentPoint[1].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[1].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CurrentPoint[2].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[2].Row		= (uint16_t)(TETRIS_ROW_NUMBER+1);
			MovingObject.CurrentPoint[3].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2-1);
			MovingObject.CurrentPoint[3].Row		= (uint16_t)(TETRIS_ROW_NUMBER+1);
			MovingObject.CenterPoint.Column		= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CenterPoint.Row			= (uint16_t)(TETRIS_ROW_NUMBER);
		}
		break;
		case S_Block:
		{
			MovingObject.CurrentPoint[0].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2-1);
			MovingObject.CurrentPoint[0].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CurrentPoint[1].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[1].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CurrentPoint[2].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[2].Row		= (uint16_t)(TETRIS_ROW_NUMBER+1);
			MovingObject.CurrentPoint[3].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2+1);
			MovingObject.CurrentPoint[3].Row		= (uint16_t)(TETRIS_ROW_NUMBER+1);
			MovingObject.CenterPoint.Column		= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CenterPoint.Row			= (uint16_t)(TETRIS_ROW_NUMBER);
		}
		break;
		case T_Block:
		{
			MovingObject.CurrentPoint[0].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2+1);
			MovingObject.CurrentPoint[0].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CurrentPoint[1].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[1].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CurrentPoint[2].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CurrentPoint[2].Row		= (uint16_t)(TETRIS_ROW_NUMBER+1);
			MovingObject.CurrentPoint[3].Column	= (uint16_t)(TETRIS_COLUMN_NUMBER/2-1);
			MovingObject.CurrentPoint[3].Row		= (uint16_t)(TETRIS_ROW_NUMBER);
			MovingObject.CenterPoint.Column		= (uint16_t)(TETRIS_COLUMN_NUMBER/2);
			MovingObject.CenterPoint.Row			= (uint16_t)(TETRIS_ROW_NUMBER);
		}
		break;
	}

	MovingObject.NextTetromino[0] = MovingObject.NextTetromino[1];
	MovingObject.NextTetromino[1] = MovingObject.NextTetromino[2];
	if(++Current_Bag_No >= 7)
	{
		Current_Bag_No = 0;
		if(CurrentBag == BlockBag1)
		{
			CurrentBag = BlockBag2;
			NextBag = BlockBag1;
		}
		else
		{
			CurrentBag = BlockBag1;
			NextBag = BlockBag2;
		}
		BlockBag_Rand(NextBag);
	}
	MovingObject.NextTetromino[2] = CurrentBag[Current_Bag_No];

	MovingObject.Check();
	return 0;
}

int MovingObject_MoveLeft()
{
	int i;
	Coordinate_t VirtualBlock[4];

	if(MovingObject.able_moveLeft == 0) return 1;	//Unable to move Left
	for(i = 0; i < 4; i++)
	{
		VirtualBlock[i].Column = MovingObject.CurrentPoint[i].Column - 1;
		VirtualBlock[i].Row = MovingObject.CurrentPoint[i].Row;
	}
	memcpy(MovingObject.CurrentPoint, VirtualBlock, sizeof(Coordinate_t)*4);
	MovingObject.CenterPoint.Column--;
	MovingObject.Check();
	return 0;	// Successfully
}

int MovingObject_MoveRight()
{
	int i;
	Coordinate_t VirtualBlock[4];

	if(MovingObject.able_moveRight == 0) return 1;	// Unable to move Right
	for(i = 0; i < 4; i++)
	{
		VirtualBlock[i].Column = MovingObject.CurrentPoint[i].Column + 1;
		VirtualBlock[i].Row = MovingObject.CurrentPoint[i].Row;
	}
	memcpy(MovingObject.CurrentPoint, VirtualBlock, sizeof(Coordinate_t)*4);
	MovingObject.CenterPoint.Column++;
	MovingObject.Check();
	return 0;	// Successfully
}

int MovingObject_MoveDown()
{
	if(MovingObject.able_moveDown == 1)
	{
		Coordinate_t VirtualBlock[4];
		for(int i = 0; i < 4; i++)
		{
			VirtualBlock[i].Column = MovingObject.CurrentPoint[i].Column;
			VirtualBlock[i].Row = MovingObject.CurrentPoint[i].Row - 1;
		}
		memcpy(MovingObject.CurrentPoint, VirtualBlock, sizeof(Coordinate_t)*4);
		MovingObject.CenterPoint.Row--;
		MovingObject.Check();
		return 0;	// Successfully
	}
	return 1;	// The game need Update!
}

int MovingObject_Rotate()
{
	Coordinate_t VirtualBlock[4];
	uint8_t _able_Rotate = 1;
//	uint8_t _Boundary_Left = 0;
//	uint8_t _Boundary_Right = 0;
	/* Rotate Left: (CenterPoint = CurrentPoint[1])
	 * DestPoint.Column = CenterPoint.Column + (SrcPoint.Row - CenterPoint.Row);
	 * DestPoint.Row = CenterPoint.Row - (SrcPoint.Column - CenterPoint.Column);
	 * */

	for(int i = 0; i < 4; i++)
	{
		VirtualBlock[i].Column = (int16_t)( MovingObject.CenterPoint.Column + (MovingObject.CurrentPoint[i].Row - MovingObject.CenterPoint.Row) );
		VirtualBlock[i].Row = (int16_t)( MovingObject.CenterPoint.Row - (MovingObject.CurrentPoint[i].Column - MovingObject.CenterPoint.Column) );
		if( (VirtualBlock[i].Row < 0) || (VirtualBlock[i].Column < 0) || (VirtualBlock[i].Column >= TETRIS_COLUMN_NUMBER) )
		{
			_able_Rotate = 0;
			break;
		}
		if(StandingObject.is_Existed(VirtualBlock[i]))
		{
			_able_Rotate = 0;
			break;
		}
//		else if(VirtualBlock[i].Column < 0)
//			_Boundary_Left = 1;
//		else if(VirtualBlock[i].Column >= TETRIS_COLUMN_NUMBER)
//			_Boundary_Right = 1;
	}

	if(_able_Rotate == 1)
	{
		memcpy(MovingObject.CurrentPoint, VirtualBlock, sizeof(Coordinate_t)*4);
		MovingObject.Check();
		return 0;
	}

	// TODO
	return 1;
}

int MovingObject_Check()
{
	MovingObject.CheckAbleMove();
	MovingObject.CalculateDropPlace();
	return 0;
}

int MovingObject_CheckAbleMove()
{
	Coordinate_t VirtualBlock[4];
	// Check availability to move left
	MovingObject.able_moveLeft = 1;
	for(int i = 0; i < 4; i++)
	{
		VirtualBlock[i].Column = MovingObject.CurrentPoint[i].Column - 1;
		VirtualBlock[i].Row = MovingObject.CurrentPoint[i].Row;
		if(VirtualBlock[i].Column < 0)
		{
			MovingObject.able_moveLeft = 0;	// Unable to move left because boundary reached
			break;
		}
		if(StandingObject.is_Existed(VirtualBlock[i]))
		{
			MovingObject.able_moveLeft = 0;	// Unable to move left because of collision
			break;
		}
	}

	// Check availability to move right
	MovingObject.able_moveRight = 1;
	for(int i = 0; i < 4; i++)
	{
		VirtualBlock[i].Column = MovingObject.CurrentPoint[i].Column + 1;
		VirtualBlock[i].Row = MovingObject.CurrentPoint[i].Row;
		if(VirtualBlock[i].Column >= TETRIS_COLUMN_NUMBER)
		{
			MovingObject.able_moveRight = 0;	// Unable to move right because boundary reached
			break;
		}
		if(StandingObject.is_Existed(VirtualBlock[i]))
		{
			MovingObject.able_moveRight = 0;	// Unable to move right because of collision
			break;
		}
	}

	// Check availability to move down
	MovingObject.able_moveDown = 1;
	for(int i = 0; i < 4; i++)
	{
		VirtualBlock[i].Column = MovingObject.CurrentPoint[i].Column;
		VirtualBlock[i].Row = MovingObject.CurrentPoint[i].Row - 1;
		if(VirtualBlock[i].Row < 0)
		{
			MovingObject.able_moveDown = 0;	// Unable to move down because boundary reached
			break;
		}
		if(StandingObject.is_Existed(VirtualBlock[i]))
		{
			MovingObject.able_moveDown = 0;	// Unable to move down because of collision
			break;
		}
	}
	return 0;
}

int MovingObject_CalculateDropPlace()
{
	uint8_t _able_moveDown = 1;
	Coordinate_t VirtualBlock[4];
	Coordinate_t CurrentDropPoint[4];

	memcpy(CurrentDropPoint, MovingObject.CurrentPoint, sizeof(Coordinate_t)*4);

	while(_able_moveDown == 1)
	{
		for(int i = 0; i < 4; i++)
		{
			VirtualBlock[i].Column = CurrentDropPoint[i].Column;
			VirtualBlock[i].Row = CurrentDropPoint[i].Row - 1;
			if(VirtualBlock[i].Row < 0)
			{
				_able_moveDown = 0;	// Unable to move down because boundary reached
				break;
			}
			if(StandingObject.is_Existed(VirtualBlock[i]))
			{
				_able_moveDown = 0;	// Unable to move down because of collision
				break;
			}
		}
		if(_able_moveDown == 1)
			memcpy(CurrentDropPoint, VirtualBlock, sizeof(Coordinate_t)*4);
	}
	memcpy(MovingObject.DropPoint ,CurrentDropPoint, sizeof(Coordinate_t)*4);
	return 0;
}

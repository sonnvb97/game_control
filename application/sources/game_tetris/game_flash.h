#ifndef GAME_FLASH_H
#define GAME_FLASH_H

#include "flash.h"
#include "app_flash.h"

typedef struct
{
	const void		*pVariable;
	const uint32_t	size;
	uint32_t	address;
} User_Flash_t;

typedef struct
{
	uint32_t		StartAddress;
	User_Flash_t	*Table;
} Flash_t;

extern unsigned int	Random_Number;
extern uint16_t		Tetris_HighScore[5];
extern void game_flash_init();
extern void game_flash_read();
extern void game_flash_write();

#endif // GAME_FLASH_H

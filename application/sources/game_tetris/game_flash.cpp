#include "game_flash.h"

unsigned int	Random_Number;
uint16_t		Tetris_HighScore[5];

#define		_NULL	(void*)0

User_Flash_t Game_Flash_Table[] =
{
	{&Random_Number,		sizeof(unsigned int)	},
	{Tetris_HighScore,		sizeof(uint16_t)*5		},
	{_NULL}
};

Flash_t Game_Flash =
{
	APP_FLASH_GAMES,
	Game_Flash_Table
};

void game_flash_init()
{
	if(Game_Flash.Table[0].pVariable == _NULL)
		return;

	Game_Flash.Table[0].address = Game_Flash.StartAddress;

	int i = 1;
	while(Game_Flash.Table[i].pVariable != _NULL)
	{
		Game_Flash.Table[i].address = (uint32_t)(Game_Flash.Table[i-1].address + Game_Flash.Table[0].size);
		i++;
	}
}

void game_flash_read()
{
	int i = 0;
	while(Game_Flash.Table[i].pVariable != _NULL)
	{
		flash_read(Game_Flash.Table[i].address, (uint8_t *)Game_Flash.Table[i].pVariable, Game_Flash.Table[i].size);
		i++;
	}
}

void game_flash_write()
{
	flash_erase_sector(APP_FLASH_GAMES);
	int i = 0;
	while(Game_Flash.Table[i].pVariable != _NULL)
	{
		flash_write(Game_Flash.Table[i].address, (uint8_t *)Game_Flash.Table[i].pVariable, Game_Flash.Table[i].size);
		i++;
	}
}

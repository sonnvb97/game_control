#include "Tetris_GameHandler.h"
#include "app.h"
#include "tetris_struct.h"

uint16_t	Tetris_Score = 0;
uint8_t		Tetris_Level = 1;
uint8_t		Tetris_Line  = 0;

uint32_t	Soft_Time = 1000;	//1000
uint32_t	Hard_Time = 3500;
uint32_t	Down_Time = 50;

void Level_Update()
{
	uint8_t _LevelTemp = Tetris_Line/10+1;
	if(_LevelTemp != Tetris_Level)
	{
		Tetris_Level = _LevelTemp;
		switch(Tetris_Level)
		{
			case 1:
				Soft_Time = 1000;
				break;
			case 2:
				Soft_Time = 800;
				break;
			case 3:
				Soft_Time = 600;
				break;
			case 4:
				Soft_Time = 500;
				break;
			case 5:
				Soft_Time = 400;
				break;
			case 6:
				Soft_Time = 300;
				break;
			case 7:
				Soft_Time = 200;
				break;
			case 8:
				Soft_Time = 150;
				break;
			case 9:
				Soft_Time = 100;
				break;
			case 10:
				Soft_Time = 75;
				break;
			default:
				Soft_Time = 50;
				break;
		}
	}
}

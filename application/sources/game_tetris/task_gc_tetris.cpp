#include "task_gc_tetris.h"
#include "app.h"
#include "task_list.h"

#include "my_debug.h"
#include "game_flash.h"

#include "app_bsp.h"
#include "timer.h"

static void Timer_Control();
static void Tetris_Down();

static void Tetris_Init();
static void Tetris_DeInit();
static void Tetris_Button_Up();
static void Tetris_Button_Down();
static void Tetris_Button_Left();
static void Tetris_Button_Right();
static void Tetris_Soft_Time_Out();
static void Tetris_Hard_Time_Out();

void task_gc_tetris(ak_msg_t* message)
{
	switch(message->sig)
	{
		case TETRIS_GC_INIT_REQ:
			task_post_pure_msg(TETRIS_TASK_UI_ID, TETRIS_UI_INIT_REQ);
			break;
		case TETRIS_GC_START_GAME_REQ:
			Tetris_Init();
			task_post_pure_msg(TETRIS_TASK_UI_ID, TETRIS_UI_GAME_START);
			task_post_pure_msg(TETRIS_TASK_SM_ID, GAME_SM_START_GAME_RES_OK);
			break;
		case TETRIS_GC_GO_LEFT:
			Tetris_Button_Left();
			break;
		case TETRIS_GC_GO_RIGHT:
			Tetris_Button_Right();
			break;
		case TETRIS_GC_ROTATE:
			Tetris_Button_Up();
			break;
		case TETRIS_GC_GO_DOWN:
			Tetris_Button_Down();
			break;
		case TETRIS_GC_CONTROL_SOFT_TIME:
			debug_info("%10s: Soft Time!\n", "[GC]");
			Tetris_Soft_Time_Out();
			break;
		case TETRIS_GC_CONTROL_HARD_TIME:
			debug_info("%10s: Hard Time!\n", "[GC]");
			Tetris_Hard_Time_Out();
			break;
	}
}

void Timer_Control()
{
	if(MovingObject.able_moveDown == false)
	{
		timer_remove_attr(TETRIS_TASK_GC_ID, TETRIS_GC_CONTROL_SOFT_TIME);
		timer_set(TETRIS_TASK_GC_ID, TETRIS_GC_CONTROL_SOFT_TIME, Soft_Time, TIMER_ONE_SHOT);
	}
}

void Tetris_Init()
{
	debug_info("%10s: Start to Read Flash!\n", "[GC]");
	game_flash_init();
	game_flash_read();
	debug_info("%10s: Flash Read: %d!\n", "[GC]", Random_Number);
	srand(Random_Number);
	Random_Number = rand();
	game_flash_write();
	debug_info("%10s: Flash Write: %d!\n", "[GC]", Random_Number);
	game_flash_read();
	debug_info("%10s: Flash Read: %d!\n", "[GC]", Random_Number);

	StandingObject.Init();
	MovingObject.Init();
	Tetris_Score = 0;
	Tetris_Line = 0;
	Tetris_Level = 1;

	timer_set(TETRIS_TASK_GC_ID, TETRIS_GC_CONTROL_SOFT_TIME, Soft_Time, TIMER_ONE_SHOT);
	timer_set(TETRIS_TASK_GC_ID, TETRIS_GC_CONTROL_HARD_TIME, Hard_Time, TIMER_ONE_SHOT);
}

void Tetris_DeInit()
{
	MovingObject.DeInit();
	StandingObject.DeInit();
}

void Tetris_Button_Up()
{
	if(MovingObject.Rotate() == 0)
	{
		Timer_Control();
		task_post_pure_msg(TETRIS_TASK_UI_ID, TETRIS_UI_UPDATE);
	}
}

void Tetris_Down()
{
	if(MovingObject.MoveDown() == 1)
	{
		if(StandingObject.Update() == 1)	//Game Over
		{
			Tetris_DeInit();
			task_post_pure_msg(TETRIS_TASK_SM_ID, GAME_SM_GAME_OVER);
			task_post_pure_msg(TETRIS_TASK_UI_ID, TETRIS_UI_GAME_OVER);
			return;
		}
		else
		{
			MovingObject.Generate();
		}
	}
	timer_set(TETRIS_TASK_GC_ID, TETRIS_GC_CONTROL_SOFT_TIME, Soft_Time, TIMER_ONE_SHOT);
	timer_set(TETRIS_TASK_GC_ID, TETRIS_GC_CONTROL_HARD_TIME, Hard_Time, TIMER_ONE_SHOT);
	task_post_pure_msg(TETRIS_TASK_UI_ID, TETRIS_UI_UPDATE);
}

void Tetris_Button_Down()
{
	if((btn_mode.state != BUTTON_SW_STATE_RELEASED) && (btn_down.state != BUTTON_SW_STATE_RELEASED))
	{
		timer_remove_attr(TETRIS_TASK_GC_ID, TETRIS_GC_CONTROL_SOFT_TIME);
		timer_remove_attr(TETRIS_TASK_GC_ID, TETRIS_GC_CONTROL_HARD_TIME);

		Tetris_Down();
		timer_set(TETRIS_TASK_GC_ID, TETRIS_GC_GO_DOWN, Down_Time, TIMER_ONE_SHOT);
	}
}

void Tetris_Button_Left()
{
	if(MovingObject.able_moveLeft)
	{
		MovingObject.MoveLeft();
		Timer_Control();
		task_post_pure_msg(TETRIS_TASK_UI_ID, TETRIS_UI_UPDATE);
	}
}

void Tetris_Button_Right()
{
	if(MovingObject.able_moveRight)
	{
		MovingObject.MoveRight();
		Timer_Control();
		task_post_pure_msg(TETRIS_TASK_UI_ID, TETRIS_UI_UPDATE);
	}
}

void Tetris_Soft_Time_Out()
{
	timer_remove_attr(TETRIS_TASK_GC_ID, TETRIS_GC_CONTROL_HARD_TIME);
	Tetris_Down();
}

void Tetris_Hard_Time_Out()
{
	timer_remove_attr(TETRIS_TASK_GC_ID, TETRIS_GC_CONTROL_SOFT_TIME);
	Tetris_Down();
}

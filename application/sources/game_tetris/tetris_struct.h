#ifndef __TETRIS_STRUCT_H__
#define __TETRIS_STRUCT_H__

#include "tetris_gameconfig.h"
#include "tetris_coordinate.h"

#include "ak.h"
#include "message.h"
#include "task.h"

typedef int(*TetrisFunction_t)(void);
typedef void(*GameFunction_t)(void);
typedef uint32_t	TetrisRow_t;

typedef enum
{
	I_Block = 0,
	J_Block,
	L_Block,
	O_Block,
	S_Block,
	T_Block,
	Z_Block
} Tetromino_t;

typedef struct
{
	TetrisFunction_t	Init;
	TetrisFunction_t	DeInit;
	TetrisFunction_t	Generate;
	TetrisFunction_t	MoveLeft;
	TetrisFunction_t	MoveRight;
	TetrisFunction_t	MoveDown;
	TetrisFunction_t	Rotate;
	TetrisFunction_t	Check;
	TetrisFunction_t	CheckAbleMove;
	TetrisFunction_t	CalculateDropPlace;

	Tetromino_t			NextTetromino[3];
	Coordinate_t		CurrentPoint[4];
	fCoordinate_t		CenterPoint;
	Coordinate_t		DropPoint[4];

	uint8_t				able_moveLeft;
	uint8_t				able_moveRight;
	uint8_t				able_moveDown;
} MovingObject_t;

typedef struct
{
	TetrisFunction_t	Init;
	TetrisFunction_t	DeInit;
	TetrisFunction_t	Update;

	uint8_t(*is_Existed)(Coordinate_t);

	TetrisRow_t			Row[TETRIS_ROW_NUMBER+4];	// 4 Virtual Row!!
} StandingObject_t;

//typedef struct
//{
//	pf_task			Task_SM_Playing;
//	pf_task			Task_SM_Game_Over;
//	pf_task			Task_GC;
//	pf_task			Task_UI;

//	GameFunction_t	Init;
//	GameFunction_t	DeInit;
//	GameFunction_t	Button_Up;
//	GameFunction_t	Button_Down;
//	GameFunction_t	Button_Left;
//	GameFunction_t	Button_Right;

//	GameFunction_t	GraphicInit;
//	GameFunction_t	GraphicClear;
//	GameFunction_t	GraphicUpdate;
//	GameFunction_t	GraphicShow;
//} GameHandler_t;

extern uint16_t		Tetris_Score;
extern uint8_t		Tetris_Level;
extern uint8_t		Tetris_Line;

extern uint32_t	Soft_Time;
extern uint32_t	Hard_Time;
extern uint32_t	Down_Time;

#endif // __TETRIS_STRUCT_H__

#ifndef __TETRIS_STANDINGOBJECT_H__
#define __TETRIS_STANDINGOBJECT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "tetris_gameconfig.h"
#include "tetris_struct.h"

extern StandingObject_t StandingObject;

#ifdef __cplusplus
}
#endif

#endif // __TETRIS_STANDINGOBJECT_H__

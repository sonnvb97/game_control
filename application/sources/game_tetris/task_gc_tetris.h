#ifndef __TASK_GC_TETRIS_H__
#define __TASK_GC_TETRIS_H__

#include "tetris_struct.h"
#include "Tetris_MovingObject.h"
#include "Tetris_StandingObject.h"

#include "ak.h"
#include "message.h"
#include "task.h"

void task_gc_tetris(ak_msg_t* message);

#endif // __TASK_GC_TETRIS_H__
